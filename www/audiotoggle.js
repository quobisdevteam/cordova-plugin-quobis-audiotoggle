var exec = require('cordova/exec');

exports.SPEAKER = 'speaker';
exports.EARPIECE = 'earpiece';
exports.NORMAL = 'normal';
exports.RINGTONE = 'ringtone';

exports.setAudioMode = function (mode) {
	exec(null, null, 'AudioTogglePlugin', 'setAudioMode', [mode]);
};

exports.isBluetoothConnected = function (success, error) {
	exec(success, error, 'AudioTogglePlugin', 'isBluetoothConnected', []);
};

exports.startCall = function () {
	exec(null, null, 'AudioTogglePlugin', 'startCall', []);
};

exports.endCall = function () {
	exec(null, null, 'AudioTogglePlugin', 'endCall', []);
};
