## AudioToggle

Modified version of https://github.com/alongubkin/audiotoggle
adding features from https://github.com/Quobis/cordova-plugin-force-speaker.git

Cordova plugin for switching between speaker and earpiece when playing audio.

    cordova plugin add com.dooble.audiotoggle
    
Integrates the detection of the audio route changes in iOS.
    
### Supported Platforms

- Android
- iOS

### Usage

To set the current audio mode, use the `setAudioMode` method:

    AudioToggle.setAudioMode(AudioToggle.SPEAKER);
    // or
    AudioToggle.setAudioMode(AudioToggle.EARPIECE);

Android has the following additional options:

    AudioToggle.setAudioMode(AudioToggle.NORMAL);
    // and
    AudioToggle.setAudioMode(AudioToggle.RINGTONE);