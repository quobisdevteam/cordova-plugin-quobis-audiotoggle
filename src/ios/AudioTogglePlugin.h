#import <Cordova/CDVPlugin.h>

@interface AudioTogglePlugin : CDVPlugin

@property BOOL earpiece;

- (void)setAudioMode:(CDVInvokedUrlCommand*)command;

- (void)isBluetoothConnected:(CDVInvokedUrlCommand*)command;

- (void)startCall:(CDVInvokedUrlCommand*)command;

- (void)endCall:(CDVInvokedUrlCommand*)command;

@end
