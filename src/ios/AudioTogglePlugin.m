#import "AudioTogglePlugin.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@implementation AudioTogglePlugin

@synthesize earpiece;

- (void)pluginInitialize {
    NSLog(@"Initializing AudioToggle plugin");
    self.earpiece = NO;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                          selector:@selector(routeChange:)
                                          name:AVAudioSessionRouteChangeNotification
                                          object:nil];
    [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker
                                                       error:nil];
    NSLog(@"AudioToggle plugin initialized");
}

- (void) isBluetoothConnected:(CDVInvokedUrlCommand *)command {
    NSString* callbackId = command.callbackId;
    BOOL resultBoolean = false;

    AVAudioSessionRouteDescription *currentRoute = [[AVAudioSession sharedInstance] currentRoute];
    for (AVAudioSessionPortDescription *output in currentRoute.outputs) {
        if ([output.portType isEqualToString:@"BluetoothA2DPOutput"] ||
            [output.portType isEqualToString:@"BluetoothHFP"]) {
            resultBoolean = true;
        }
    }
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:resultBoolean];
    [self.commandDelegate sendPluginResult:result callbackId:callbackId];
}

- (void)setAudioMode:(CDVInvokedUrlCommand *)command {
    NSError* __autoreleasing err = nil;
    NSString* mode = [NSString stringWithFormat:@"%@", [command.arguments objectAtIndex:0]];

    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_None;
    AVAudioSession *session = [AVAudioSession sharedInstance];

    if ([mode isEqualToString:@"earpiece"]) {
        self.earpiece = YES;
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&err];
        audioRouteOverride = kAudioSessionProperty_OverrideCategoryDefaultToSpeaker;
        AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(audioRouteOverride), &audioRouteOverride);
    } else if ([mode isEqualToString:@"speaker"] || [mode isEqualToString:@"ringtone"]) {
        self.earpiece = NO;
        [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker |
            AVAudioSessionCategoryOptionAllowBluetoothA2DP |
            AVAudioSessionCategoryOptionAllowBluetooth
            error:&err];
    } else if ([mode isEqualToString:@"normal"]) {
        self.earpiece = NO;
        [session setCategory:AVAudioSessionCategorySoloAmbient error:&err];
    }
}

- (void)routeChange:(NSNotification*)notification {
    NSLog(@"Audio device route changed!");
    if (self.earpiece == YES) {
        return;
    }

    AVAudioSession* session = [AVAudioSession sharedInstance];
    AVAudioSessionRouteDescription *currentRoute = session.currentRoute;
    NSArray<AVAudioSessionPortDescription *> *outputs = currentRoute.outputs;
    if ([outputs count] == 0) {
        return;
    }
    AVAudioSessionPortDescription *output = [outputs objectAtIndex:0];
    NSLog(@"current output name %@ type %@", [output portName], [output portType]);

    if ([[output portType] isEqualToString:AVAudioSessionPortBuiltInReceiver]) {
        NSError* error;
        [session setActive: YES error: nil];
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
    }
}

- (void)startCall:(CDVInvokedUrlCommand *)command {

}

- (void)endCall:(CDVInvokedUrlCommand *)command {

}

@end
