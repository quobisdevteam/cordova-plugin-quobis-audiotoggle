package com.quobis.audiotoggle;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BluetoothBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction() == BluetoothDevice.ACTION_ACL_CONNECTED) {
			AudioTogglePlugin.bluetoothConnected();
		} else if (intent.getAction() == BluetoothDevice.ACTION_ACL_DISCONNECTED) {
			AudioTogglePlugin.bluetoothDisconnected();
		}
	}
}