package com.quobis.audiotoggle;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.media.AudioManager;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import static android.media.AudioManager.AUDIOFOCUS_GAIN;

public class AudioTogglePlugin extends CordovaPlugin {
	public static final String TAG = AudioTogglePlugin.class.getSimpleName();

	public static final String ACTION_IS_BLUETOOTH_CONNECTED = "isBluetoothConnected";
	public static final String ACTION_SET_AUDIO_MODE = "setAudioMode";
	public static final String ACTION_START_CALL = "startCall";
	public static final String ACTION_END_CALL = "endCall";

	public static final String EARPIECE = "earpiece";
	public static final String SPEAKER = "speaker";
	public static final String RINGTONE = "ringtone";
	public static final String NORMAL = "normal";

	public static boolean inCall = false;
	public static boolean bluetoothConnected = false;
	public static String lastAudioMode = null;
	public static AudioTogglePlugin instance = null;

	private AudioManager.OnAudioFocusChangeListener audioFocusListener;
	private BroadcastReceiver bluetoothConnectionStateReceiver;

	@Override
	public boolean execute(String action, JSONArray args,
						   CallbackContext callbackContext) throws JSONException {
		if (action.equals(ACTION_IS_BLUETOOTH_CONNECTED)) {
			callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, bluetoothConnected));
			return true;
		} else if (action.equals(ACTION_SET_AUDIO_MODE)) {
			if (!setAudioMode(args.getString(0))) {
				callbackContext.error("Invalid audio mode");
				return false;
			}
			return true;
		} else if (action.equals(ACTION_START_CALL)) {
			startCall();
			return true;
		} else if (action.equals(ACTION_END_CALL)) {
			endCall();
			return true;
		}
		callbackContext.error("Invalid action:" + action);
		return false;
	}

	public boolean isBluetoothConnected() {
		return bluetoothConnected;
	}

	public boolean setAudioMode(String mode) {
		Log.d(TAG, "setAudioMode(" + mode + ")");

		lastAudioMode = mode;
		if (bluetoothConnected) {
			Log.d(TAG, "setAudioMode(): bluetooth is connected, ignoring");
			return false;
		}
		AudioManager audioManager = (AudioManager) cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
		if (mode.equals(EARPIECE)) {
			audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
			audioManager.setSpeakerphoneOn(false);
			return true;
		} else if (mode.equals(SPEAKER)) {
			audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
			audioManager.setSpeakerphoneOn(true);
			return true;
		} else if (mode.equals(RINGTONE)) {
			audioManager.setMode(AudioManager.MODE_RINGTONE);
			audioManager.setSpeakerphoneOn(false);
			return true;
		} else if (mode.equals(NORMAL)) {
			audioManager.setMode(AudioManager.MODE_NORMAL);
			audioManager.setSpeakerphoneOn(false);
			return true;
		}
		return false;
	}

	private void requestAudioFocus() {
		audioFocusListener = new AudioManager.OnAudioFocusChangeListener() {
			@Override
			public void onAudioFocusChange(int i) {
			}
		};
		AudioManager audioManager = (AudioManager) cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
		audioManager.requestAudioFocus(audioFocusListener, AudioManager.STREAM_VOICE_CALL, AUDIOFOCUS_GAIN);
	}

	public void startCall() {
		Log.d(TAG, "startCall()");

		instance = this;
		inCall = true;

		cordova.getActivity().setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
		requestAudioFocus();

		if (bluetoothConnected) {
			startSendingAudioToBluetooth();
		}
	}

	public void endCall() {
		Log.d(TAG, "endCall()");

		inCall = false;
		instance = null;

		if (bluetoothConnected) {
			stopSendingAudioToBluetooth();
		}

		cordova.getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);
		AudioManager audioManager = (AudioManager) cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
		audioManager.abandonAudioFocus(audioFocusListener);
	}

	private void startSendingAudioToBluetooth() {
		Log.d(TAG, "startSendingAudioToBluetooth()");

		AudioManager audioManager = (AudioManager) cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
		audioManager.setBluetoothScoOn(true);
		audioManager.startBluetoothSco();
		audioManager.setSpeakerphoneOn(false);
		audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
	}

	private void stopSendingAudioToBluetooth() {
		Log.d(TAG, "stopSendingAudioToBluetooth()");

		AudioManager audioManager = (AudioManager) cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
		audioManager.setBluetoothScoOn(false);

		if (lastAudioMode != null) {
			setAudioMode(lastAudioMode);
		}
	}

	static void bluetoothConnected() {
		Log.d(TAG, "bluetoothConnected()");

		bluetoothConnected = true;
		if (inCall && instance != null) {
			instance.startSendingAudioToBluetooth();
		}
	}

	static void bluetoothDisconnected() {
		Log.d(TAG, "bluetoothDisconnected()");

		bluetoothConnected = false;
		if (inCall && instance != null) {
			instance.stopSendingAudioToBluetooth();
		}
	}
}
